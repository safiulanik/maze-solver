"""
The function below finds the shortest path from a given starting
position in a maze to a given target. If a path is not available
(when there are no way to reach the target or the target is not
present in the maze), the function returns the string "Target is
unreachable". In the maze, a value of 0(zero) in a cell defines
a way through and a value of 1 defines a roadblock (which can be
any other value as well if passed as a parameter).

Since this algorithm is going through each cell once, time
complexity of this algorithm is O(MN) where M and N are the
number of rows and the number of columns respectively.

For each cell, the path from starting position is stored in a
dictionary. And maximum length of each record can be the number of
total cells. So the space complexity is roughly O(MN*MN).
"""

def find_path(x, y, target, maze, roadblock=1):
    """
    :param x: starting position row index
    :param y: starting position column index
    :param target: value to find in the maze
    :param maze: the maze description as a 2D list
    :param roadblock: value that will be used as blocking position
    :return: shortest path from starting position to target
    """
    row_count = len(maze)
    col_count = len(maze[0]) # assuming maze has at least 1 column

    path_dict = {}
    for i in range(row_count):
        for j in range(col_count):
            # index of the dictionary is a tuple consisting of the position
            path_dict[(i, j)] = [(i, j)]  # insert each position as its starting position in a list

    visited = [[False] * col_count for i in range(row_count)]
    visited[x][y] = True  # mark starting position as visited

    neighbour_rows = [-1, 0, 0, 1]  # needed to check neighbouring cells
    neighbour_cols = [0, -1, 1, 0]  # needed to check neighbouring cells

    queue = [(x, y)]  # insert starting position to the queue
    while len(queue) > 0:
        current_x, current_y = queue.pop(0)

        if maze[current_x][current_y] == target:
            return path_dict[(current_x, current_y)][::-1]

        for i in range(4):
            next_x = current_x + neighbour_rows[i]
            next_y = current_y + neighbour_cols[i]

            if 0 <= next_x < row_count and 0 <= next_y < col_count \
                    and maze[next_x][next_y] != roadblock \
                    and visited[next_x][next_y] is False:
                visited[next_x][next_y] = True
                queue.append((next_x, next_y))
                path_dict[(next_x, next_y)].extend(path_dict[(current_x, current_y)])

    return "Target is unreachable"


# Testing
import unittest


class TestFindPath(unittest.TestCase):
    def setUp(self):
        self.error_msg = 'Wrong Output'
        self.mazes = [
            [
                [1, 1, 0, 1],
                [0, 0, 1, 2],
                [0, 0, 0, 0]
            ], [
                [1, 1, 0, 1],
                [0, 0, 1, 2],
                [0, 0, 0, 0]
            ], [
                [2, 1, 0, 1],
                [0, 0, 1, 0],
                [0, 0, 0, 0]
            ], [
                [0, 0, 0, 1],
                [0, 1, 1, 0],
                [0, 0, 0, 2]
            ], [
                [0, 0],
                [0, 1],
                [0, 2],
                [0, 0]
            ]
        ]
        self.starting_positions = [
            (0, 2),
            (1, 0),
            (0, 0),
            (0, 1),
            (0, 1)
        ]
        self.expected_outputs = [
            'Target is unreachable',
            [(1, 0), (1, 1), (2, 1), (2, 2), (2, 3), (1, 3)],
            [(0, 0)],
            [(0, 1), (0, 0), (1, 0), (2, 0), (2, 1), (2, 2), (2, 3)],
            [(0, 1), (0, 0), (1, 0), (2, 0), (2, 1)]
        ]

    def test_find_path(self):
        for i in range(len(self.mazes)):
            x = self.starting_positions[i][0]
            y = self.starting_positions[i][1]
            self.assertEqual(find_path(x, y, target=2, maze=self.mazes[i]), self.expected_outputs[i], self.error_msg)

if __name__ == '__main__':
    unittest.main()
